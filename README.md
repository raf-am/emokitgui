# EmokitGUI

A Graphical User Interface (GUI) using PyQtGraph, Matplotlib and Emokit
libraries to vizualize, load, and export raw data in real time from Emotiv
Epoc+. It is an unfinished/uncomment version, and improvments are expected in
future. A few bugs are known for which suggestions are welcome. 

Special thanks to Christodoulos Benetatos [@xribene] from "Emokit_Epoc_GUI"
(https://github.com/xribene/Emokit_Epoc_GUI) and to Dorian Tovar [@datovard]
from "EmokitVisualizer" (https://github.com/EmokitAlife/EmokitVisualizer).

### Screenshots

![](screenshot1.jpg)  
![](screenshot2.jpg)  
![](screenshot3.jpg)

### Prerequisites

* Python 2.7
* Emokit python library - https://github.com/openyou/emokit
* PyQt4.8+
* pyqtgraph
* scipy
* matplotlib

### Info

A [PyInstaller](https://www.pyinstaller.org/) frozen package compiled to
Windows-based OS can be found at **./dist** directory. Unzip the
content and execute "_**EmokitGUI.exe**_" file.

### Authors

Rafael A. Mattos - @raf-am  
Thais Faria