from __future__ import division

import sys

from pyqtgraph.Qt import QtCore, QtGui
from matplotlib.figure import Figure
from matplotlib.gridspec import GridSpec
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from numpy.random import randint, random
from numpy import zeros, nan, array, round, atleast_2d, linspace, tile, square
from numpy import nanmean, nanmin, argmax, isnan, float64
from numpy import abs as npabs
from math import ceil
from collections import OrderedDict
from os.path import abspath, join, realpath, exists, dirname
from threading import Thread, Event
from collections import deque
from time import sleep
from datetime import datetime as dtt
from Queue import Queue
from scipy import fft
from scipy.signal import butter, filtfilt
from emokit.emotiv import Emotiv
from emokit.util import device_is_emotiv, hid_enumerate
from platform import system as platsys
from csv import writer as csvwrt
from warnings import filterwarnings

filterwarnings("ignore")

if platsys() == "Windows":
    import pywinusb.hid as hid
else:
    import hidapi

dtt.strptime("0", "%H")

if hasattr(sys, '_MEIPASS'):
    LOCALDIR = abspath(join(dirname(sys.executable), u""))
else:
    LOCALDIR = abspath(join(realpath(__file__), u"../"))

class HeatMapWidget(QtGui.QWidget):
    def __init__(self, parent=None):
        super(HeatMapWidget, self).__init__(parent)

        self.electrodesPosition = \
            {
                "AF3": {"x": 54, "y": 38},
                "AF4": {"x": 152, "y": 38},
                "F7" : {"x": 23, "y": 72},
                "F3" : {"x": 80, "y": 74},
                "F4" : {"x": 130, "y": 74},
                "F8" : {"x": 185, "y": 72},
                "FC5": {"x": 45, "y": 102},
                "FC6": {"x": 165, "y": 102},
                "T7" : {"x": 11, "y": 135},
                "T8" : {"x": 197, "y": 135},
                "P7" : {"x": 45, "y": 220},
                "P8" : {"x": 165, "y": 220},
                "O1" : {"x": 78, "y": 260},
                "O2" : {"x": 133, "y": 260}
            }

        layout = QtGui.QHBoxLayout()
        self.headsetState = QtGui.QLabel()
        layout.addWidget(self.headsetState)
        self.setLayout(layout)
        self.minValue = -607
        self.maxValue = 3075
        layout.setAlignment(QtCore.Qt.AlignHCenter)

        self.pixmap = QtGui.QPixmap(join(LOCALDIR, "headset.png"))

    def updateHeatMapStatus(self, packet):
        painter = QtGui.QPainter()

        if painter.begin(self.pixmap):

            painter.setFont(QtGui.QFont('Decorative', 13))
            painter.drawText(66, 165, 100, 20, QtCore.Qt.AlignCenter,
                             "signal quality")

            painter.setFont(QtGui.QFont('Decorative', 8))

            for key in self.electrodesPosition:
                if key[0] == "O":
                    painter.drawText(self.electrodesPosition[key]["x"] - 2,
                                     self.electrodesPosition[key]["y"] - 15,
                                     30, 15,
                                     QtCore.Qt.AlignCenter, key)
                elif key == "T7":
                    painter.drawText(self.electrodesPosition[key]["x"] + 7,
                                     self.electrodesPosition[key]["y"] + 24,
                                     30, 15,
                                     QtCore.Qt.AlignCenter, key)
                elif key == "T8":
                    painter.drawText(self.electrodesPosition[key]["x"] - 10,
                                     self.electrodesPosition[key]["y"] + 24,
                                     30, 15,
                                     QtCore.Qt.AlignCenter, key)
                else:
                    painter.drawText(self.electrodesPosition[key]["x"] - 2,
                                     self.electrodesPosition[key]["y"] + 25,
                                     30, 15, QtCore.Qt.AlignCenter, key)

            if packet == None:
                painter.setBrush(QtGui.QColor(80, 80, 80))
                for item in self.electrodesPosition:
                    painter.drawEllipse(self.electrodesPosition[item]["x"],
                                        self.electrodesPosition[item]["y"],
                                        24, 24)
            else:
                for sensor in packet.sensors:
                    if sensor in self.electrodesPosition:
                        color = qualicolor(packet.sensors[sensor]["quality"],
                                           norm=False)
                        painter.setBrush(QtGui.QColor(color[0], color[1],
                                                      color[2]))
                        painter.drawEllipse(
                            self.electrodesPosition[sensor]["x"],
                            self.electrodesPosition[sensor]["y"], 24, 24)
            painter.end()

        self.headsetState.setPixmap(self.pixmap)


class MatplotlibWidget(QtGui.QWidget):
    def __init__(self, size=(5.0, 6.0), dpi=100, bgcolor=(0.0, 0.0, 0.0),
                 frame=False):
        QtGui.QWidget.__init__(self)
        self.fig = Figure(size, dpi=dpi, facecolor=bgcolor, frameon=frame)
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self)

        self.vbox = QtGui.QVBoxLayout()
        self.vbox.addWidget(self.canvas)
        self.vbox.setContentsMargins(0, 0, 0, 0)

        self.setLayout(self.vbox)

    def getFigure(self):
        return self.fig

    def draw(self):
        self.canvas.draw()


class MainWindow(QtGui.QMainWindow):
    def __init__(self, electrodes, sampledt, tw, Fs, step, parent=None):
        super(MainWindow, self).__init__(parent)

        electrs = electrodes.split()
        self.electrsdc = OrderedDict((elec, {"eegax": None,
                                             "eegline": None,
                                             "eegquali": None,
                                             "fftax": None,
                                             "fftline": None,
                                             "chkbtn": None})
                                     for elec in electrs)

        self.sampledt = sampledt
        self.pltbuffsize = int((1 * 15) / self.sampledt)
        self.tw = tw
        self.Fs = Fs
        self.step = step

        self.queued = Queue()
        self.queuew = Queue()
        self.queuesp = OrderedDict((elec,
                                    RingBuffer(size=self.pltbuffsize,
                                               fill=True,
                                               elems=[0]))
                                   for elec in self.electrsdc)

        self.thrdeventp = Event()

        self.mdiwnd = QtGui.QMdiArea()
        self.setCentralWidget(self.mdiwnd)

        subwndmain = QtGui.QMdiSubWindow()

        menuframe = QtGui.QFrame()
        menuframe.setStyleSheet(
            """.QFrame {padding: -3px;}""")
        layout = QtGui.QVBoxLayout(menuframe)

        connframe = QtGui.QFrame()
        connframe.setStyleSheet(
            """background-color: rgb(240, 240, 240);""")
        layoutconn = QtGui.QVBoxLayout(connframe)

        srclbl = QtGui.QLabel("Digital Signal Source")
        srclbl.setAlignment(QtCore.Qt.AlignCenter)
        srclbl.setStyleSheet(
            """.QLabel {background-color: rgb(250, 250, 250);
                        border: 2px groove rgb(190, 190, 190);
                        border-bottom: 0px;
                        border-radius: 5px;
                        font: bold 10px;
                        color: rgb(140, 140, 140);
                        padding: 2px;
                        margin-bottom: 0px;}""")
        layoutconn.addWidget(srclbl)

        self.tabsrc = QtGui.QTabWidget()

        sensorinfo = QtGui.QWidget()
        layoutsens = QtGui.QFormLayout()
        self.snwd = QtGui.QLineEdit()
        self.snwd.setReadOnly(True)
        self.snwd.setStyleSheet("background-color:rgb(230,230,230);")
        layoutsens.addRow(QtGui.QLabel("S/N:"), self.snwd)
        self.battwd = QtGui.QLineEdit()
        self.battwd.setReadOnly(True)
        self.battwd.setStyleSheet("background-color:rgb(230,230,230);")
        layoutsens.addRow(QtGui.QLabel("Battery:"), self.battwd)
        sensorinfo.setLayout(layoutsens)

        self.tabsrc.addTab(sensorinfo, "Sensor")

        fileininfo = QtGui.QWidget()
        layoutfile = QtGui.QVBoxLayout()
        self.infilebtn = QtGui.QPushButton("Open file...")
        self.infilebtn.clicked.connect(self.getfile)
        layoutfile.addWidget(self.infilebtn)
        self.fnamein = u""
        self.fnameinwd = QtGui.QLineEdit()
        self.fnameinwd.setReadOnly(False)
        self.fnameinwd.setStyleSheet(
            """background-color:rgb(230,230,230);
               font: italic 9px""")
        layoutfile.addWidget(self.fnameinwd)
        fileininfo.setLayout(layoutfile)

        self.tabsrc.addTab(fileininfo, "History")

        layoutconn.addWidget(self.tabsrc)

        self.connbtn = QtGui.QPushButton("Disconnected")
        self.connbtn.setStyleSheet(
            """.QPushButton:checked {background-color: rgb(100,255,100);
                                    font: bold;}
               .QPushButton {font: bold;}""")
        self.connbtn.setCheckable(True)
        self.connbtn.clicked.connect(lambda: self.connect(self.connbtn))

        layoutconn.addWidget(self.connbtn)

        layout.addWidget(connframe)

        self.headmap = HeatMapWidget()
        self.headmap.updateHeatMapStatus(None)
        layout.addWidget(self.headmap, 20)

        saveframe = QtGui.QFrame()
        saveframe.setStyleSheet(
            """background-color: rgb(240, 240, 240);""")
        layoutsave = QtGui.QVBoxLayout(saveframe)

        svflbl = QtGui.QLabel("Export Data File")
        svflbl.setAlignment(QtCore.Qt.AlignCenter)
        svflbl.setStyleSheet(
            """.QLabel {background-color: rgb(250, 250, 250);
                        border: 2px groove rgb(190, 190, 190);
                        border-bottom: 0px;
                        border-radius: 5px;
                        font: bold 10px;
                        color: rgb(140, 140, 140);
                        padding: 2px;
                        margin-bottom: 0px;}""")
        layoutsave.addWidget(svflbl)

        fileoutinfo = QtGui.QWidget()
        layoutfile = QtGui.QVBoxLayout()
        layoutfile.setSpacing(0)
        layoutfile.setContentsMargins(0, 0, 0, 0)

        fileoutsvf = QtGui.QWidget()
        layoutsvf = QtGui.QHBoxLayout()
        layoutsvf.setSpacing(5)
        self.outfilebtn = QtGui.QPushButton("Save file...")
        self.outfilebtn.clicked.connect(self.savefile)
        layoutsvf.addWidget(self.outfilebtn)
        self.fnameout = u""
        self.fnameoutwd = QtGui.QLineEdit()
        self.fnameoutwd.setReadOnly(False)
        self.fnameoutwd.setStyleSheet(
            """.QLineEdit {background-color:rgb(230,230,230);
                           font: italic 9px;}""")
        layoutsvf.addWidget(self.fnameoutwd, 20)
        self.playsavebtn = QtGui.QPushButton(u"\u25CF")
        self.fnameoutwd.textChanged.connect(
            lambda ev, btn=self.playsavebtn, ln=self.fnameoutwd:
                btn.setEnabled(True) if len(ln.text()) > 0
                else btn.setEnabled(False))
        self.playsavebtn.setEnabled(False)
        self.playsavebtn.setCheckable(True)
        self.playsavebtn.clicked.connect(
            lambda: self.export(self.playsavebtn))
        self.playsavebtn.setStyleSheet(
            """.QPushButton {font: bold 26px;
                             padding-left: 8px;
                             padding-right: 8px;
                             padding-top: -4px;
                             padding-bottom: -2px;
                             margin: 0px;}
               .QPushButton:checked {color: rgb(220,0,0);}""")
        layoutsvf.addWidget(self.playsavebtn)
        fileoutsvf.setLayout(layoutsvf)
        layoutfile.addWidget(fileoutsvf)

        fileoutinfo.setLayout(layoutfile)
        layoutsave.addWidget(fileoutinfo)

        layout.addWidget(saveframe)

        menuframe.setLayout(layout)

        widgetsubmain = QtGui.QWidget()
        widgetsubmain.setStyleSheet(
            "background-color:rgb(235,235,235);margin:0px;padding:0px")
        layout = QtGui.QHBoxLayout(widgetsubmain)

        splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        splitter.addWidget(menuframe)
        splitter.addWidget(widgetsubmain)
        splitter.setStretchFactor(1, 40)
        splitter.setStyleSheet(
            """QSplitter:handle {background-color: rgb(240, 240, 240);
                                 border: 1px solid rgb(100, 100, 100);
                                 border-left: 1px solid rgb(170, 170, 170);
                                 border-style: inset;
                                 margin-right: 3px;
                                 padding-right: 3px;}""")

        self.tabwd = QtGui.QTabWidget()

        graph = QtGui.QWidget()
        layoutgraph = QtGui.QVBoxLayout(graph)
        self.mpltwdeeg = MatplotlibWidget(frame=True)
        layoutgraph.addWidget(self.mpltwdeeg, 50)

        graphopts = QtGui.QFrame()
        layoutopts = QtGui.QGridLayout(graphopts)
        layoutopts.setAlignment(QtCore.Qt.AlignLeft)
        optlb = QtGui.QLabel("Options:")
        optlb.setStyleSheet(
            """.QLabel {color: rgb(140, 140, 140);
                        font: bold;
                        padding-right: 10px;}"""
        )
        layoutopts.addWidget(optlb, 1, 1)
        self.eegfilterbtn = QtGui.QCheckBox("Filtering")
        self.eegfilterbtn.setStyleSheet(
            """.QCheckBox {padding: 1px;
                           margin-right: 10px;
                           border: 1px outset rgb(150, 150, 150);} """
        )
        layoutopts.addWidget(self.eegfilterbtn, 1, 2)
        yoptlb = QtGui.QLabel("[Y-Axis]")
        yoptlb.setStyleSheet(
            """.QLabel {font: bold;} """
        )
        layoutopts.addWidget(yoptlb, 1, 3)
        yoptminvlb = QtGui.QLabel("Min:")
        layoutopts.addWidget(yoptminvlb, 1, 4)
        self.yeegoptminv = QtGui.QSpinBox()
        self.yeegoptminv.setRange(-8000, 8000)
        self.yeegoptminv.setSingleStep(10)
        self.yeegoptminv.setValue(-200)
        layoutopts.addWidget(self.yeegoptminv, 1, 5)
        yoptmaxvlb = QtGui.QLabel("Max:")
        layoutopts.addWidget(yoptmaxvlb, 1, 6)
        self.yeegoptmaxv = QtGui.QSpinBox()
        self.yeegoptmaxv.setRange(-8000, 8000)
        self.yeegoptmaxv.setSingleStep(10)
        self.yeegoptmaxv.setValue(200)
        layoutopts.addWidget(self.yeegoptmaxv, 1, 7)
        self.yeegoptminv.valueChanged.connect(
            lambda ev, btnmi=self.yeegoptminv, btnma=self.yeegoptmaxv:
                (btnmi.setMaximum(btnma.value()),
                 btnma.setMinimum(btnmi.value())))
        self.yeegoptmaxv.valueChanged.connect(
            lambda ev, btnmi=self.yeegoptminv, btnma=self.yeegoptmaxv:
                (btnma.setMinimum(btnmi.value()),
                 btnmi.setMaximum(btnma.value())))
        self.yeegoptautobtn = QtGui.QCheckBox("Auto")
        self.yeegoptautobtn.setStyleSheet(
            """.QCheckBox {padding: 3px;} """
        )
        layoutopts.addWidget(self.yeegoptautobtn, 1, 8)
        self.yeegoptautobtn.stateChanged.connect(
            lambda ev, btn=self.yeegoptautobtn, mi=self.yeegoptminv,
            ma=self.yeegoptmaxv:
                ((mi.setEnabled(False) if btn.isChecked()
                 else mi.setEnabled(True)),
                (ma.setEnabled(False) if btn.isChecked()
                 else ma.setEnabled(True))))
        self.yeegoptautobtn.setChecked(True)
        xoptlb = QtGui.QLabel("[X-Axis]")
        xoptlb.setStyleSheet(
            """.QLabel {font: bold;}"""
        )
        layoutopts.addWidget(xoptlb, 1, 9)
        xoptscllb = QtGui.QLabel("Last:")
        layoutopts.addWidget(xoptscllb, 1, 10)
        self.eegxscale = QtGui.QComboBox()
        self.eegxscale.setStyleSheet(
            """padding-left: 4px; padding-right: 4px;
               background-color: rgb(250, 250, 250);
               selection-background-color: rgb(100, 100, 100);"""
        )
        self.eegxscale.addItems(["{:d} secs".format(s)
                                 for s in (2, 5, 10, 15)])
        self.eegxscale.setCurrentIndex(3)
        layoutopts.addWidget(self.eegxscale, 1, 11)
        graphopts.setLayout(layoutopts)

        layoutgraph.addWidget(graphopts)
        layoutgraph.setSpacing(0)
        layoutgraph.setContentsMargins(0, 0, 0, 0)
        graph.setLayout(layoutgraph)

        self.tabwd.addTab(graph, "EEG")
        self.mpltwdeeg.setStyleSheet(
            "background-color:white;margin:0px;padding:0px")

        graph = QtGui.QWidget()
        layoutgraph = QtGui.QVBoxLayout(graph)
        self.mpltwdfft = MatplotlibWidget(frame=True)
        layoutgraph.addWidget(self.mpltwdfft, 50)

        graphopts = QtGui.QFrame()
        layoutopts = QtGui.QGridLayout(graphopts)
        layoutopts.setAlignment(QtCore.Qt.AlignLeft)
        optlb = QtGui.QLabel("Options:")
        optlb.setStyleSheet(
            """.QLabel {color: rgb(140, 140, 140);
                        font: bold;
                        padding-right: 10px;}"""
        )
        layoutopts.addWidget(optlb, 1, 1)
        self.fftfilterbtn = QtGui.QCheckBox("Filtering")
        self.fftfilterbtn.setStyleSheet(
            """.QCheckBox {padding: 1px;
                           margin-right: 10px;
                           border: 1px outset rgb(150, 150, 150);} """
        )
        layoutopts.addWidget(self.fftfilterbtn, 1, 2)
        xoptlb = QtGui.QLabel("[X-Axis]")
        xoptlb.setStyleSheet(
            """.QLabel {font: bold;} """
        )
        layoutopts.addWidget(xoptlb, 1, 3)
        xoptminvlb = QtGui.QLabel("Min:")
        layoutopts.addWidget(xoptminvlb, 1, 4)
        self.xfftoptminv = QtGui.QSpinBox()
        self.xfftoptminv.setRange(0, 200)
        self.xfftoptminv.setSingleStep(10)
        self.xfftoptminv.setValue(0)
        layoutopts.addWidget(self.xfftoptminv, 1, 5)
        xoptmaxvlb = QtGui.QLabel("Max:")
        layoutopts.addWidget(xoptmaxvlb, 1, 6)
        self.xfftoptmaxv = QtGui.QSpinBox()
        self.xfftoptmaxv.setRange(0, 200)
        self.xfftoptmaxv.setSingleStep(10)
        self.xfftoptmaxv.setValue(50)
        layoutopts.addWidget(self.xfftoptmaxv, 1, 7)
        self.xfftoptminv.valueChanged.connect(
            lambda ev, btnmi=self.xfftoptminv, btnma=self.xfftoptmaxv:
                (btnmi.setMaximum(btnma.value()),
                 btnma.setMinimum(btnmi.value())))
        self.xfftoptmaxv.valueChanged.connect(
            lambda ev, btnmi=self.xfftoptminv, btnma=self.xfftoptmaxv:
                (btnma.setMinimum(btnmi.value()),
                 btnmi.setMaximum(btnma.value())))
        self.xfftoptautobtn = QtGui.QCheckBox("Auto")
        # self.xfftoptautobtn.setChecked(True)
        self.xfftoptautobtn.setStyleSheet(
            """.QCheckBox {padding: 3px;} """
        )
        layoutopts.addWidget(self.xfftoptautobtn, 1, 8)
        self.xfftoptautobtn.stateChanged.connect(
            lambda ev, btn=self.xfftoptautobtn, mi=self.xfftoptminv,
            ma=self.xfftoptmaxv:
                ((mi.setEnabled(False) if btn.isChecked()
                 else mi.setEnabled(True)),
                (ma.setEnabled(False) if btn.isChecked()
                 else ma.setEnabled(True))))
        self.xfftoptautobtn.setChecked(True)
        graphopts.setLayout(layoutopts)

        self.eegfilterbtn.stateChanged.connect(
            lambda ev, btne=self.eegfilterbtn, btnf=self.fftfilterbtn:
                btnf.setChecked(btne.isChecked()))
        self.fftfilterbtn.stateChanged.connect(
            lambda ev, btne=self.eegfilterbtn, btnf=self.fftfilterbtn:
                btne.setChecked(btnf.isChecked()))
        self.eegfilterbtn.setChecked(True)

        layoutgraph.addWidget(graphopts)
        layoutgraph.setSpacing(0)
        layoutgraph.setContentsMargins(0, 0, 0, 0)
        graph.setLayout(layoutgraph)

        self.tabwd.addTab(graph, "FFT")
        self.mpltwdfft.setStyleSheet(
            "background-color:white;margin:0px;padding:0px")
        # widgetsubmain.setStyleSheet(
        #     "background-color:rgb(235,235,235);margin:0px;padding:0px")

        self.buildaxes(electrs)

        self.mpltwdeeg.getFigure().subplots_adjust(left=0.05, bottom=0.05,
                                                   right=0.95, top=0.95,
                                                   wspace=0.2, hspace=0.2)
        self.mpltwdeeg.draw()

        self.mpltwdfft.getFigure().subplots_adjust(left=0.035, bottom=0.05,
                                                   right=0.95, top=0.95,
                                                   wspace=0.25, hspace=0.3)
        self.mpltwdfft.draw()

        self.eegxscale.currentIndexChanged.connect(
            self.setxeegscale)


        datav = QtGui.QWidget()
        layoutdatav = QtGui.QVBoxLayout(datav)

        self.dataviewd = QtGui.QPlainTextEdit()
        self.dataviewd.setReadOnly(True)
        self.dataviewd.setStyleSheet(
            """.QPlainTextEdit {background-color: rgb(0, 0, 0);
                                color: rgb(255, 255, 255);
                                font: 14px bold;}""")
        font = QtGui.QFont("Monospace")
        font.setStyleHint(QtGui.QFont.TypeWriter)
        self.dataviewd.setFont(font)
        layoutdatav.addWidget(self.dataviewd)

        datavopts = QtGui.QFrame()
        layoutdatavopts = QtGui.QGridLayout(datavopts)
        layoutdatavopts.setAlignment(QtCore.Qt.AlignLeft)
        optlb = QtGui.QLabel("Options:")
        optlb.setStyleSheet(
            """.QLabel {color: rgb(140, 140, 140);
                        font: bold;
                        padding-right: 10px;}"""
        )
        layoutdatavopts.addWidget(optlb, 1, 1)
        xoptscllb = QtGui.QLabel("Fontsize:")
        layoutdatavopts.addWidget(xoptscllb, 1, 2)
        self.datavfsize = QtGui.QComboBox()
        self.datavfsize.setStyleSheet(
            """padding-left: 4px; padding-right: 4px;
               background-color: rgb(250, 250, 250);
               selection-background-color: rgb(100, 100, 100);"""
        )
        self.datavfsize.addItems(["{:d} px".format(s)
                                 for s in (8, 10, 12, 14, 16, 18, 22, 26, 30)])
        self.datavfsize.setCurrentIndex(3)
        self.datavfsize.currentIndexChanged.connect(
            lambda: self.dataviewd.setStyleSheet(
                """.QPlainTextEdit {background-color: rgb(0, 0, 0);
                                    color: rgb(255, 255, 255);""" +
                "font: {:s}px bold;".format(
                    self.datavfsize.itemText(
                        self.datavfsize.currentIndex()).split()[0]) +
                "}"))
        layoutdatavopts.addWidget(self.datavfsize, 1, 3)

        layoutdatav.addWidget(datavopts)
        layoutdatav.setSpacing(0)
        layoutdatav.setContentsMargins(0, 0, 0, 0)

        datav.setLayout(layoutdatav)
        self.tabwd.addTab(datav, "DATA")

        self.plotter = Plotter(
            self.electrsdc,
            {"tabwd": self.tabwd,
             "battwd": self.battwd,
             "snwd": self.snwd,
             "yeegoptautobtn": self.yeegoptautobtn,
             "yeegoptminv": self.yeegoptminv,
             "yeegoptmaxv": self.yeegoptmaxv,
             "xfftoptautobtn": self.xfftoptautobtn,
             "xfftoptminv": self.xfftoptminv,
             "xfftoptmaxv": self.xfftoptmaxv,
             "mpltwdeeg": self.mpltwdeeg,
             "mpltwdfft": self.mpltwdfft,
             "eegfilterbtn": self.eegfilterbtn,
             "fftfilterbtn": self.fftfilterbtn,
             "eegxscale": self.eegxscale,
             "headmap": self.headmap,
             "playsavebtn": self.playsavebtn,
             "dataviewd": self.dataviewd,
             "datavfsize": self.datavfsize,
             },
            self.Fs,
            self.tw,
            self.step,
            self.sampledt,
            self.pltbuffsize,
            self.queued,
            self.queuew,
            self.queuesp,
            self.thrdeventp)

        layout.addWidget(self.tabwd, 14)

        eegslctwd = QtGui.QWidget()
        grideegslct = QtGui.QGridLayout(eegslctwd)

        self.chkbtnelecall = QtGui.QCheckBox("All")
        self.chkbtnelecall.setStyleSheet(
            "color:rgb(75,75,75);font:bold 14px;")
        self.chkbtnelecall.setChecked(True)
        self.chkbtnelecall.clicked.connect(
            lambda: self.btnstate(self.chkbtnelecall))
        grideegslct.addWidget(self.chkbtnelecall, 1, 1)

        hline = QtGui.QFrame()
        hline.setFrameShape(QtGui.QFrame.HLine)
        hline.setFrameShadow(QtGui.QFrame.Sunken)
        grideegslct.addWidget(hline, 2, 1)

        for eidx, elec in enumerate(electrs):
            self.electrsdc[elec]["chkbtn"] = QtGui.QCheckBox(elec)
            self.electrsdc[elec]["chkbtn"].setStyleSheet("font:normal 12px;")
            self.electrsdc[elec]["chkbtn"].setChecked(True)
            self.electrsdc[elec]["chkbtn"].clicked.connect(
                lambda ev, el=elec: self.btnstate(self.electrsdc[el]["chkbtn"]))
            grideegslct.addWidget(self.electrsdc[elec]["chkbtn"], eidx + 3, 1)

        eegslctwd.setLayout(grideegslct)
        eegslctwd.setStyleSheet("background-color:rgb(235,235,235);")

        layout.addWidget(eegslctwd, 1)

        widgetsubmain.setLayout(layout)

        subwndmain.setWidget(splitter)
        subwndmain.setWindowTitle("Main")

        self.mdiwnd.addSubWindow(subwndmain)
        subwndmain.setWindowState(QtCore.Qt.WindowMaximized)
        subwndmain.setWindowFlags(QtCore.Qt.WindowMinMaxButtonsHint)
        subwndmain.show()

        # self.mdiwnd.tileSubWindows()

        bar = self.menuBar()
        file = bar.addMenu("File")
        file.addAction("Reset")
        file.addAction("Quit")
        file.triggered[QtGui.QAction].connect(self.windowaction)
        self.setWindowTitle("EmokitGUI")
        self.statusBar = QtGui.QStatusBar()
        self.statusBar.setStyleSheet(
            """.QStatusBar {border: 2px inset rgb(170, 170, 170);
                            padding: 0px;}"""
        )
        self.statusBar.setMaximumHeight(12)
        self.setStatusBar(self.statusBar)

        # self.timer = QtCore.QTimer()
        # self.timer.timeout.connect(self.update)

    def export(self, btn):
        if btn.isChecked():
            self.outfilebtn.setEnabled(False)
            self.fnameoutwd.setReadOnly(True)

            self.writethrd = WriterFile(self.queuew, self.electrsdc,
                                        self.fnameoutwd.text())
            self.writethrd.start()
        else:
            self.outfilebtn.setEnabled(True)
            self.fnameoutwd.setReadOnly(False)

            self.writethrd.stop()

    def getfile(self):
        self.fnamein = QtGui.QFileDialog.getOpenFileName(
            self, "Select file", "C:\\", "CSV files (*.csv)")[0]
        self.fnameinwd.setText(self.fnamein)

    def savefile(self):
        self.fnameout = QtGui.QFileDialog.getSaveFileName(
            self, "Select file", "C:\\", "CSV files (*.csv)")[0]
        self.fnameoutwd.setText(self.fnameout)

    def validdialog(self, tabtext, errmsg):
        if tabtext == "Sensor":
            msginfo = "Check if sensor is connected on a right way."
        elif tabtext == "History":
            msginfo = "Check if history file exist or filename is corrected."

        msg = QtGui.QMessageBox()
        msg.setIcon(QtGui.QMessageBox.Warning)

        msg.setText("Fail to connect!\n\n" + msginfo)
        msg.setWindowTitle("Connect Error")
        msg.setDetailedText(str(errmsg))
        msg.setStandardButtons(QtGui.QMessageBox.Ok)

        retval = msg.exec_()

        return retval

    def connect(self, btn):
        if btn.text() == "Disconnected":
            try:
                self.okconn = self.validconn(
                    self.tabsrc.tabText(self.tabsrc.currentIndex()))
            except Exception as err:
                retval = self.validdialog(
                    self.tabsrc.tabText(self.tabsrc.currentIndex()),
                    err)
                btn.setChecked(False)
                self.okconn = False
            else:
                if(self.tabsrc.tabText(self.tabsrc.currentIndex()) ==
                   "Sensor"):
                    self.tabsrc.setTabEnabled(1, False)

                    self.readthrd = ReaderData(self.queued, self.thrdeventp,
                                               self.step, self.Fs, self.tw,
                                               self.sampledt, self.electrsdc,
                                               Emotiv,
                                               {"display_output": False,
                                                "verbose": False,
                                                "write": False})
                    self.readthrd.start()
                elif(self.tabsrc.tabText(self.tabsrc.currentIndex()) ==
                     "History"):
                    self.tabsrc.setTabEnabled(0, False)
                    self.infilebtn.setEnabled(False)
                    self.fnameinwd.setReadOnly(True)

                    self.readthrd = ReaderData(self.queued, self.thrdeventp,
                                               self.step, self.Fs, self.tw,
                                               self.sampledt, self.electrsdc,
                                               open,
                                               {"name": self.fnameinwd.text(),
                                                "mode": "r"})
                    self.readthrd.start()

            if self.okconn:
                btn.setText("Connected")
                self.plotter.start(0.5 * self.step * self.sampledt * 1000.)
        else:
            btn.setText("Disconnected")
            self.tabsrc.setTabEnabled(0, True)
            self.tabsrc.setTabEnabled(1, True)
            self.snwd.setText("")
            self.battwd.setText("")
            self.infilebtn.setEnabled(True)
            self.fnameinwd.setReadOnly(False)

            if self.okconn:
                self.readthrd.stop()
                self.plotter.stop()

    def validconn(self, tabtext):
        if tabtext == "Sensor":
            if platsys() != "Windows":
                hidapi.hid_init()
                path, serialnumber = hid_enumerate(hidapi, platsys())
                if len(path) == 0:
                    raise Exception("Device not found!")
            else:
                devices = []
                for device in hid.find_all_hid_devices():
                    if device_is_emotiv(device, platsys()):
                        devices.append(device)
                if len(devices) == 0:
                    raise Exception("Device not found!")
                else:
                    device = devices[1]
                    device.open()
                    serialnumber = device.serial_number
                    device.close()

            self.snwd.setText(serialnumber)
        elif tabtext == "History":
            with open(self.fnameinwd.text()) as headset:
                headset.readline()

        return True

    def griddims(self, totlen, maxrows=8):

        ncols = 1 + totlen // (maxrows + 1)
        nrows = int(ceil(totlen / float(ncols)))

        return nrows, ncols

    def setxeegscale(self, idx):
        idxn = int(self.eegxscale.itemText(idx).split()[0])

        for eleck, elecv in self.electrsdc.iteritems():
            lent = len(elecv["eegline"][0].get_ydata())
            elecv["eegax"].relim()
            elecv["eegax"].set_xlim(
                [self.pltbuffsize - int((idxn) / self.sampledt),
                 self.pltbuffsize])

        self.mpltwdeeg.draw()

    def buildaxes(self, electrs):
        nrows, ncols = self.griddims(len(electrs), 8)
        gseeg = GridSpec(nrows, ncols)

        nrows, ncols = self.griddims(len(electrs), 5)
        gsfft = GridSpec(nrows, ncols)

        for eidx, elec in enumerate(electrs):
            subp = self.mpltwdeeg.getFigure().add_subplot(
                gseeg[eidx],
                facecolor=(0.2, 0.2, 0.2), frameon=True)
            subp.yaxis.tick_right()
            subp.yaxis.set_label_position("left")
            subp.set_ylabel(elec, color=(1.0, 1.0, 1.0), rotation=0,
                            va="center", ha="right", labelpad=2)
            for (spinek, spine) in subp.spines.iteritems():
                spine.set_color((0.8, 0.8, 0.8))
                if spinek != "right":
                    spine.set_color((0.0, 0.0, 0.0))
                else:
                    spine.set_color((0.8, 0.8, 0.8))
            for label in subp.xaxis.get_ticklabels():
                label.set_color((0.8, 0.8, 0.8))
                label.set_fontsize(8.0)
            for label in subp.yaxis.get_ticklabels():
                label.set_color((0.8, 0.8, 0.8))
                label.set_fontsize(8.0)
            subp.tick_params(axis='both',
                             colors=(0.8, 0.8, 0.8),
                             direction="out")

            self.electrsdc[elec]["eegline"] = (
                subp.plot(zeros(shape=self.pltbuffsize), "r-", lw=0.5))
            self.electrsdc[elec]["eegquali"] = (
                subp.text(0.5, 0.875, u"", ha='center', va='bottom',
                          transform=subp.transAxes,
                          fontsize=8, color=(1.0, 1.0, 1.0),
                          bbox=dict(facecolor=(0.25, 0.25, 0.25),
                                    edgecolor=(0.65, 0.65, 0.65),
                                    alpha=0.75,
                                    boxstyle="round",
                                    pad=0.25)))
            self.electrsdc[elec]["eegquali"].set_visible(False)
            idxn = int(self.eegxscale.itemText(
                self.eegxscale.currentIndex()).split()[0])
            subp.set_xlim(
                [self.pltbuffsize - int((idxn) / self.sampledt),
                 self.pltbuffsize])
            subp.get_xaxis().set_visible(False)
            subp.get_yaxis().set_visible(False)

            self.electrsdc[elec]["eegax"] = subp

            subp = self.mpltwdfft.getFigure().add_subplot(
                gsfft[eidx],
                facecolor=(0.2, 0.2, 0.2), frameon=True)
            subp.yaxis.tick_right()
            subp.yaxis.set_ticks([])
            subp.yaxis.set_label_position("left")
            subp.set_ylabel(elec, color=(1.0, 1.0, 1.0), rotation=0,
                            va="center", ha="right", labelpad=2)
            for (spinek, spine) in subp.spines.iteritems():
                spine.set_color((0.8, 0.8, 0.8))
                if spinek != "right" and spinek != "bottom":
                    spine.set_color((0.0, 0.0, 0.0))
                else:
                    spine.set_color((0.8, 0.8, 0.8))
            for label in subp.xaxis.get_ticklabels():
                label.set_color((0.8, 0.8, 0.8))
                label.set_fontsize(8.0)
            for label in subp.yaxis.get_ticklabels():
                label.set_color((0.8, 0.8, 0.8))
                label.set_fontsize(8.0)
                label.set_visible(False)
            subp.tick_params(axis='both',
                             colors=(0.8, 0.8, 0.8),
                             direction="in")

            self.electrsdc[elec]["fftline"] = (
                subp.plot(zeros(shape=self.Fs), "-", lw=0.5,
                          color=(0.6, 0.6, 0.6)))
            self.electrsdc[elec]["fftmax"] = (
                subp.text(0., 0., u"", ha='center', va='top',
                          fontweight="bold",
                          fontsize=8, color=(0.5, 0.5, 1.0),
                          bbox=dict(facecolor=(0.1, 0.1, 0.1),
                                    edgecolor=(0.4, 0.4, 0.4),
                                    alpha=0.95,
                                    boxstyle="round",
                                    pad=0.25)))
            self.electrsdc[elec]["fftmax"].set_visible(False)
            subp.get_xaxis().set_visible(False)
            subp.get_yaxis().set_visible(False)

            self.electrsdc[elec]["fftax"] = subp

    def btnstate(self, btn):

        if btn.text() == "All":
            for eleck, elecv in self.electrsdc.iteritems():
                elecv["chkbtn"].setChecked(btn.isChecked())

            if btn.isChecked():
                elecchk = len(self.electrsdc)
            else:
                elecchk = 1
        else:
            elecchk = len([eleck for eleck, elecv in self.electrsdc.iteritems()
                        if elecv["chkbtn"].isChecked()])

        if elecchk != len(self.electrsdc):
            self.chkbtnelecall.setChecked(False)

        nrows, ncols = self.griddims(elecchk, 8)
        gseeg = GridSpec(nrows, ncols)
        nrows, ncols = self.griddims(elecchk, 5)
        gsfft = GridSpec(nrows, ncols)

        gsidx = 0

        for eleck, elecv in self.electrsdc.iteritems():
            if elecv["chkbtn"].isChecked():
                elecv["eegax"].set_position(
                    gseeg[gsidx].get_position(self.mpltwdeeg.getFigure()))
                elecv["eegax"].set_visible(True)

                elecv["fftax"].set_position(
                    gsfft[gsidx].get_position(self.mpltwdfft.getFigure()))
                elecv["fftax"].set_visible(True)

                gsidx += 1
            else:
                elecv["eegax"].set_visible(False)
                elecv["fftax"].set_visible(False)

        if self.tabwd.currentIndex() == 0:
            self.mpltwdeeg.draw()
        elif self.tabwd.currentIndex() == 1:
            self.mpltwdfft.draw()

    def closeEvent(self, event):
        event.accept()
        self.appquit()

    def windowaction(self, q):
        if q.text() == "Quit":
            self.resetapp()
            self.appquit()
        elif q.text() == "Reset":
            self.resetapp()

    def resetapp(self):
        if self.connbtn.isChecked():
            self.connbtn.click()

        if self.playsavebtn.isChecked():
            self.playsavebtn.click()

        self.fnameoutwd.setText("")
        self.fnameinwd.setText("")

        self.dataviewd.setPlainText("")

        self.headmap.updateHeatMapStatus(None)

        for eleck, elecv in self.electrsdc.iteritems():
            elecv["eegline"][0].set_ydata(
                zeros(shape=self.pltbuffsize))
            elecv["eegax"].get_yaxis().set_visible(False)
            elecv["eegquali"].set_visible(False)
            elecv["eegline"][0].set_color('r')
            elecv["eegax"].relim()
            elecv["eegax"].autoscale()

            elecv["fftline"][0].set_ydata(
                zeros(shape=self.Fs))
            elecv["fftax"].get_xaxis().set_visible(False)
            elecv["fftax"].get_yaxis().set_visible(False)
            elecv["fftmax"].set_visible(False)
            elecv["fftax"].relim()
            elecv["fftax"].autoscale()

        self.mpltwdeeg.draw()
        self.mpltwdfft.draw()

    def appquit(self):
        QtCore.QCoreApplication.instance().quit()
        sys.exit()


class RingBuffer(object):
    def __init__(self, size, fill=False, elems=None):
        self.size = size
        if fill:
            self._buffered = deque(self.size * elems, self.size)
        else:
            self._buffered = deque([], self.size)

    def write(self, value):
        self._buffered.append(value)

    def writeex(self, value):
        self._buffered.extend(value)

    def show(self):
        print(self._buffered)

    def copy(self, overlap):
        return list(self._buffered)[:overlap]

    def calls(self):
        return self.write.calls

    def listret(self):
        return list(self._buffered)


class Packet(object):
    def __init__(self, head="", data=""):
        self.sensors = {}
        self.timestamp = None
        self.battery = None

        for hfield, dfield in zip(head.split(','), data.split(',')):
            if "timestamp" in hfield.lower():
                if "." in dfield:
                    self.timestamp = dtt.strptime(dfield,
                                                  "%Y-%m-%d %H:%M:%S.%f")
                elif len(dfield):
                    self.timestamp = dtt.strptime(dfield,
                                                  "%Y-%m-%d %H:%M:%S")
            else:
                try:
                    dfieldv = float(dfield)
                except:
                    dfieldv = nan

                try:
                    if hfield.split()[0] in self.sensors:
                        self.sensors[
                            hfield.split()[0]][hfield.split()[1].lower()] = (
                                dfieldv
                        )
                    else:
                        self.sensors[hfield.split()[0]] = {
                            hfield.split()[1].lower(): dfieldv
                        }
                except:
                    pass


class ReaderData(Thread):
    def __init__(self, dataqueue, thrdevent, pltstep, Fs, tw, sampledt,
                 electrodes, readfunc, readfuncargs):
        super(ReaderData, self).__init__()
        self.Fs = Fs
        self._stop = Event()
        self.dataqueue = dataqueue
        self.thrdevent = thrdevent
        self.pltstep = pltstep
        self.tw = tw * self.Fs
        self.N = self.tw
        self.sampledt = sampledt
        self.electrodes = electrodes
        self.readfunc = readfunc
        self.readfuncargs = readfuncargs

        self.b = zeros(shape=(len(self.electrodes), nextpow(self.N)))

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()

    def run(self):
        databuffer = RingBuffer(self.tw,
                                fill=True,
                                elems=[(len(self.electrodes) *
                                        [[0., 0., None, None]])])
        stepcount = 0
        timespent = 0

        with self.readfunc(**self.readfuncargs) as headset:

            if "mode" in self.readfuncargs:
                headline = headset.readline()

            while not self._stop.isSet():
                tinit = dtt.now()
                stepcount += 1
                data = []

                if "mode" in self.readfuncargs:
                    ln = headset.readline()
                    packet = Packet(headline, ln)
                else:
                    packet = headset.dequeue()

                for name in self.electrodes.keys():
                    if packet is not None and name in packet.sensors:
                        data.append([packet.sensors[name]['value'],
                                     packet.sensors[name]['quality'],
                                     packet.timestamp,
                                     packet.battery])
                    else:
                        data.append([nan, 0, None, None])

                databuffer.write(data)

                if stepcount == self.pltstep:
                    y = array(databuffer.listret()).transpose(1, 0, 2)

                    battdata = y[0, :, 3]
                    battnotnone = array(map(lambda x: x is not None,
                                        battdata))
                    if battnotnone.any():
                        batterydata = int(nanmin(battdata[battnotnone]))
                    else:
                        batterydata = None

                    timedata = y[:, :, 2]
                    qualidata = y[:, :, 1]
                    eegdata = y[:, :, 0]

                    eegdatapad = pad(eegdata, self.b)
                    fftdata, freqsd = eegfft(eegdatapad, self.Fs)

                    eegfilt = filtering(eegdata, self.Fs, cut_off=[2,30],
                                        mode='band')
                    eegfiltpad = pad(eegfilt, self.b)
                    fftfilt, freqs = eegfft(eegfiltpad, self.Fs)

                    self.dataqueue.put({
                        "batterydata": batterydata,
                        "timedata": timedata,
                        "qualidata": qualidata,
                        "eegdata": eegdata,
                        "eegfilt": eegfilt,
                        "fftdata": fftdata,
                        "fftfilt": fftfilt,
                        "freqs": freqs})

                    timespent += (dtt.now() - tinit).total_seconds()

                    tt = dtt.now()
                    if(timespent < self.sampledt * self.pltstep and
                       "mode" in self.readfuncargs):
                        sleep(2 * (self.sampledt * self.pltstep - timespent))

                    timespent = 0
                    stepcount = 0

                timespent += (dtt.now() - tinit).total_seconds()

            if "mode" not in self.readfuncargs:
                headset.stop()
                sleep(0.00001)


class WriterFile(Thread):
    def __init__(self, dataqueue, electrodes, filehist):
        super(WriterFile, self).__init__()
        self._stop = False
        self.dataqueue = dataqueue
        self.electrodes = electrodes
        self.filehist = filehist
        self.writehead = True

    def stop(self):
        self.dataqueue.put([])

    def makehead(self):
        headelecs = []
        [headelecs.extend(["{:s} Value".format(eleck),
                           "{:s} Quality".format(eleck),
                           "{:s} FreqPeak".format(eleck)])
         for eleck in self.electrodes]

        return ["Timestamp"] + headelecs

    def makebody(self, data):
        body = []
        for tidx, tstamp in enumerate(data["Timestamp"]):
            if isinstance(tstamp, dtt):
                bodyline = []
                [bodyline.extend(
                    ["{:.8f}".format(data[eleck]["Value"][tidx]),
                     "{:.8f}".format(data[eleck]["Quality"][tidx]),
                     "{:.4f}".format(data[eleck]["FreqPeak"][tidx])])
                 for eleck in self.electrodes]
                body.append(["{:%Y-%m-%d %H:%M:%S.%f}".format(tstamp)] + bodyline)

        return body

    def run(self):

        if exists(self.filehist):
            self.writehead = False

        with open(self.filehist, "ab") as hfile:
            wrt = csvwrt(hfile)

            if self.writehead:
                wrt.writerow(self.makehead())
                hfile.flush()

            while not self._stop:
                data = self.dataqueue.get(block=True)
                if data:
                    body = self.makebody(data)
                    if body:
                        wrt.writerows(body)
                        hfile.flush()
                else:
                    self._stop = True


class Plotter(object):
    def __init__(self, electrodes, widgets, Fs, tw, step, sampledt, pltbuffsize,
                 dataqueue, wrtqueue, pltqueues, thrdevent):
        self.electrsdc = electrodes
        self.widgets = widgets
        self.dataqueue = dataqueue
        self.wrtqueue = wrtqueue
        self.pltqueues = pltqueues
        self.thrdevent = thrdevent
        self.step = step
        self.pltbuffsize = pltbuffsize
        self.sampledt = sampledt
        self.Fs = Fs

        self.packet = Packet()

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.checkdata)

    def newwbuffer(self):
        wbuffer = {eleck: {"Value": [],
                           "Quality": [],
                           "FreqPeak": []} for eleck in self.electrsdc}
        wbuffer["Timestamp"] = []

        return wbuffer

    def start(self, interval):
        self.timer.start(interval)

    def stop(self):
        self.timer.stop()
        for eleck, elecv in self.electrsdc.iteritems():
            self.pltqueues[eleck].writeex(self.pltbuffsize * [0])
        while self.dataqueue.qsize():
            self.dataqueue.get()

    def updateplots(self):
        wbuffer = self.newwbuffer()

        while self.dataqueue.qsize():
            data = self.dataqueue.get(block=True)

            self.widgets["battwd"].setText(
                "???" if data["batterydata"] is None
                else "{:d}".format(data["batterydata"]))

            wbuffer["Timestamp"].extend(
                list(data["timedata"][0, -self.step:]))

            for eidx, (eleck, elecv) in enumerate(self.electrsdc.iteritems()):
                if self.widgets["eegfilterbtn"].isChecked():
                    eeglst = list(data["eegfilt"][eidx, :])
                else:
                    eeglst = list(data["eegdata"][eidx, :])

                self.pltqueues[eleck].writeex(eeglst)

                self.packet.sensors[eleck] = {
                    "quality": nanmean(data["qualidata"].astype(
                        dtype=float64)[eidx, :])}

                wbuffer[eleck]["Value"].extend(eeglst[-self.step:])
                wbuffer[eleck]["Quality"].extend(
                    list(data["qualidata"].astype(
                        dtype=float64)[eidx, -self.step:]))

            self.widgets["headmap"].updateHeatMapStatus(self.packet)

        for eidx, (eleck, elecv) in enumerate(self.electrsdc.iteritems()):

            if self.widgets["fftfilterbtn"].isChecked():
                arg = argmax(data["fftfilt"][eidx, :])
            else:
                arg = argmax(data["fftdata"][eidx, :])

            fftmaxx = data["freqs"][eidx, :][arg]

            wbuffer[eleck]["FreqPeak"].extend(len(wbuffer["Timestamp"]) *
                                              [fftmaxx])

            if self.widgets["tabwd"].currentIndex() == 0:
                qualimean = nanmean(data["qualidata"][eidx, :].astype(
                    dtype=float64))
                qcolor = qualicolor(qualimean, norm=True)

                elecv["eegax"].get_yaxis().set_visible(True)
                elecv["eegline"][0].set_ydata(self.pltqueues[eleck].listret())
                elecv["eegline"][0].set_color(qcolor)
                elecv["eegquali"].set_text("{:.0f}".format(qualimean))
                elecv["eegquali"].set_color(qcolor)
                elecv["eegquali"].set_visible(True)
                elecv["eegax"].relim()
                idxn = int(self.widgets["eegxscale"].itemText(
                    self.widgets["eegxscale"].currentIndex()).split()[0])
                elecv["eegax"].set_xlim(
                    [self.pltbuffsize - int((idxn) / self.sampledt),
                     self.pltbuffsize])
                if self.widgets["yeegoptautobtn"].isChecked():
                    elecv["eegax"].autoscale(axis='y')
                else:
                    elecv["eegax"].set_ylim(
                        [int(self.widgets["yeegoptminv"].value()),
                         int(self.widgets["yeegoptmaxv"].value())])

                if eidx == len(self.electrsdc) - 1:
                    self.widgets["mpltwdeeg"].draw()

            elif self.widgets["tabwd"].currentIndex() == 1:
                elecv["fftax"].get_xaxis().set_visible(True)
                elecv["fftax"].get_yaxis().set_visible(True)
                elecv["fftline"][0].set_xdata(data["freqs"][eidx, :])
                if self.widgets["fftfilterbtn"].isChecked():
                    elecv["fftline"][0].set_ydata(data["fftfilt"][eidx, :])
                    fftmaxy = data["fftfilt"][eidx, :][arg]
                else:
                    elecv["fftline"][0].set_ydata(data["fftdata"][eidx, :])
                    fftmaxy = data["fftdata"][eidx, :][arg]
                elecv["fftmax"].set_text("{:.1f} Hz".format(fftmaxx))
                elecv["fftmax"].set_position((fftmaxx, fftmaxy))
                elecv["fftax"].relim()
                if self.widgets["xfftoptautobtn"].isChecked():
                    elecv["fftax"].autoscale()
                else:
                    elecv["fftax"].autoscale(axis='y')
                    elecv["fftax"].set_xlim(
                        [int(self.widgets["xfftoptminv"].value()),
                         int(self.widgets["xfftoptmaxv"].value())])
                if(elecv["fftax"].get_xlim()[0] < fftmaxx <
                   elecv["fftax"].get_xlim()[1]):
                    elecv["fftmax"].set_visible(True)
                else:
                    elecv["fftmax"].set_visible(False)

                if eidx == len(self.electrsdc) - 1:
                    self.widgets["mpltwdfft"].draw()

        if self.widgets["tabwd"].currentIndex() == 2:
            self.widgets["dataviewd"].setPlainText(
                self.updateout(wbuffer))

        if self.widgets["playsavebtn"].isChecked():
            self.wrtqueue.put(wbuffer)

    def updateout(self, wbuffer):

        outdct = {"serial_number": self.widgets["snwd"].text(),
                  "time_stamp": wbuffer["Timestamp"][-1]}

        outdct.update({eleck + "_value": wbuffer[eleck]["Value"][-1]
                       for eleck in self.electrsdc})
        outdct.update({eleck + "_quality": wbuffer[eleck]["Quality"][-1]
                       for eleck in self.electrsdc})
        outdct.update({eleck + "_freqpeak": wbuffer[eleck]["FreqPeak"][-1]
                       for eleck in self.electrsdc})

        tsstr = ("???" if wbuffer["Timestamp"][-1] is None
                 else "{time_stamp:%Y-%m-%d %H:%M:%S}")

        outhead = """
   [Emokit]  Timestamp: {:s} - S/N: {:s}
   +======================================================+
   |  Sensor  |     Value    |   Quality    |  Freq Peak  |
   +----------+--------------+--------------+-------------+\n""".format(
            tsstr, "{serial_number}")

        outsensor = "".join(
            ["   |    " + "{:>3s}".format(eleck) + "   " +
             "|  {" + "{:s}_value:>10.5f".format(eleck)  + "}  " +
             "|  {" + "{:s}_quality:>10.5f".format(eleck)  + "}  " +
             "|    {" + "{:s}_freqpeak:>5.2f".format(eleck)  + "}    |\n"
             for eleck in self.electrsdc])

        outsensor = outsensor.replace(" nan ", " ??? ")

        outfoot = "   +======================================================+"

        outstr = (outhead + outsensor + outfoot).format(**outdct)

        return outstr

    def checkdata(self):
        if self.dataqueue.qsize():
            self.updateplots()


def filtering(eeg, Fs, cut_off=[2,40], mode='band', order=4, ex=[111,111]):
    eegin = atleast_2d(eeg.copy())
    [C, N] = eegin.shape
    tmp = zeros(shape=eegin.shape)
    b, a = butter(order, array((cut_off)) / (Fs / 2), btype=mode)
    for idx in xrange(C):
        if (idx == ex[0]) or (idx==ex[1]):
            tmp[idx, :] = eegin[idx, :]
            continue
        tmp[idx, :] = filtfilt(b, a, eegin[idx, :])

    return tmp


def eegfft(y, Fs=128) :
    y = atleast_2d(y)
    [C, N] = y.shape
    T = 1 / Fs
    yfft = fft(y)
    xfft = linspace(0.0, 1.0 / (2.0 * T), N / 2, endpoint=False)
    xfft = atleast_2d(xfft)
    xfft = tile(xfft, (C, 1))
    yfft = (square(2.0 / N * npabs(yfft[:, 0:N//2])))

    return yfft, xfft


def pad(arrayin, arrayout):
    # zero pad array so that shape(arrayin)=shape(arrayout)
    arrayfill = arrayout.copy()
    [a, b] = arrayin.shape
    [k, l] = arrayout.shape
    start1 = int((k - a) / 2)
    start2 = int((l - b) / 2)
    arrayfill[start1:(start1+a), start2:(start2+b)] = arrayin

    return arrayfill

def nextpow(x):
    return 1 << (x - 1).bit_length()


def qualicolor(value, minvalue=0, maxvalue=5100, norm=False):
    div = int((maxvalue - 1 * minvalue)  / 255)
    quali = (value - 1 * minvalue) // div
    quali = (quali < 255) * quali + (quali > 255) * 255 + (quali == 255) * 255

    normv = 255. if norm else 1.

    if quali <= 85:
        return ((255. * quali / 85.) / normv, 0., 0.)
    elif 85 < quali <= 170:
        return (255. / normv, (255. * (quali - 85.) / (170. - 85.)) / normv, 0.)
    else:
        return ((765. - (255. / 85. * quali)) / normv, 255. / normv, 0.)


def main():
    app = QtGui.QApplication(sys.argv)
    QtGui.QApplication.setStyle(QtGui.QStyleFactory.create('Fusion'))
    app.setQuitOnLastWindowClosed(False)
    ex = MainWindow(
        electrodes="F3 FC5 F7 T7 P7 O1 O2 P8 T8 F8 AF4 FC6 F4 AF3",
        sampledt=0.0075,
        tw=2,
        Fs=128,
        step=int(round(1.0 * 128)))
    ex.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
